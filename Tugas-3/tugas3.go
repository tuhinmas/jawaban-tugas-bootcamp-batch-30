package main

import (
	"fmt"
	"strconv"
)

func main() {
	// soal 1
	fmt.Println("------SOAL 1-------")
	var panjangPersegiPanjang string = "8"
	var lebarPersegiPanjang string = "5"

	var alasSegitiga string = "6"
	var tinggiSegitiga string = "7"

	panjang, _ := strconv.Atoi(panjangPersegiPanjang)
	lebar, _ := strconv.Atoi(lebarPersegiPanjang)
	alas, _ := strconv.Atoi(alasSegitiga)
	tinggi, _ := strconv.Atoi(tinggiSegitiga)

	var luasPersegiPanjang int
	var kelilingPersegiPanjang int
	var luasSegitiga int

	luasPersegiPanjang = panjang * lebar
	kelilingPersegiPanjang = 2 * (panjang + lebar)
	luasSegitiga = alas * tinggi / 2

	fmt.Println("Luas Persegi Panjang dari panjang",
		panjang, "dan lebar",
		lebar, "adalah",
		luasPersegiPanjang)

	fmt.Println("Keliling Persegi Panjang dari panjang",
		panjang, "dan lebar",
		lebar, "adalah",
		kelilingPersegiPanjang)

	fmt.Println("Luas Segitiga dari alas",
		alas, "dan tinggi",
		tinggi, "adalah",
		luasSegitiga)

	// soal 2
	fmt.Println("------SOAL 2-------")

	var nilaiJohn = 80
	var nilaiDoe = 50

	if nilaiJohn >= 80 {
		fmt.Println("Nilai John adalah A")
	} else if nilaiJohn >= 70 && nilaiJohn < 80 {
		fmt.Println("Nilai John adalah B")
	} else if nilaiJohn >= 60 && nilaiJohn < 70 {
		fmt.Println("Nilai John adalah C")
	} else if nilaiJohn >= 50 && nilaiJohn < 60 {
		fmt.Println("Nilai John adalah D")
	} else {
		fmt.Println("Nilai John adalah E")
	}

	if nilaiDoe >= 80 {
		fmt.Println("Nilai Doe adalah A")
	} else if nilaiDoe >= 70 && nilaiDoe < 80 {
		fmt.Println("Nilai Doe adalah B")
	} else if nilaiDoe >= 60 && nilaiDoe < 70 {
		fmt.Println("Nilai Doe adalah C")
	} else if nilaiDoe >= 50 && nilaiDoe < 60 {
		fmt.Println("Nilai Doe adalah D")
	} else {
		fmt.Println("Nilai Doe adalah E")
	}

	// soal 3
	fmt.Println("------SOAL 3-------")

	var tanggal = 17
	var bulan = 8
	var tahun = 1945

	switch bulan {
	case 1:
		fmt.Println(tanggal, "Januari", tahun)
	case 2:
		fmt.Println(tanggal, "Februari", tahun)
	case 3:
		fmt.Println(tanggal, "Maret", tahun)
	case 4:
		fmt.Println(tanggal, "April", tahun)
	case 5:
		fmt.Println(tanggal, "Mei", tahun)
	case 6:
		fmt.Println(tanggal, "Juni", tahun)
	case 7:
		fmt.Println(tanggal, "Juli", tahun)
	case 8:
		fmt.Println(tanggal, "Agustus", tahun)
	case 9:
		fmt.Println(tanggal, "September", tahun)
	case 10:
		fmt.Println(tanggal, "Oktober", tahun)
	case 11:
		fmt.Println(tanggal, "November", tahun)
	case 12:
		fmt.Println(tanggal, "Desember", tahun)
	default:
		fmt.Println("inputan bulan salah")
	}

	// soal 4
	fmt.Println("------SOAL 4-------")

	switch {
	case tahun >= 1944 && tahun <= 1964:
		fmt.Println("Baby Boomer")
	case tahun >= 1965 && tahun <= 1979:
		fmt.Println("Generasi X")
	case tahun >= 1980 && tahun <= 1994:
		fmt.Println("Generasi Y (Millenials)")
	case tahun >= 1995 && tahun <= 2015:
		fmt.Println("Generasi Z")
	}
}
