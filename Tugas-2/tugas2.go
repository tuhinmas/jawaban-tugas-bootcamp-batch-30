package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// soal 1
	fmt.Println("-------SOAL 1-------")

	firstWord := "Bootcamp"
	secondWord := "Digital"
	thirdWord := "Skill"
	fourthWord := "Sanbercode"
	fifthWord := "Golang"

	fmt.Println(firstWord, secondWord, thirdWord, fourthWord, fifthWord)

	// soal 2
	fmt.Println("-------SOAL 2-------")
	halo := "Halo Dunia"

	fmt.Println(strings.Replace(halo, "Dunia", "Golang", 1))

	// soal 3
	fmt.Println("-------SOAL 3-------")

	var kataPertama = "saya"
	var kataKedua = "senang"
	var kataKetiga = "belajar"
	var kataKeempat = "golang"

	kataKedua = strings.Title(kataKedua)
	kataKetiga = kataKetiga[0:len(kataKetiga)-1] + strings.ToUpper(string(kataKetiga[len(kataKetiga)-1]))
	kataKeempat = strings.ToUpper(kataKeempat)

	fmt.Println(kataPertama, kataKedua, kataKetiga, kataKeempat)

	// soal 4
	fmt.Println("-------SOAL 4-------")
	var angkaPertama = "8"
	var angkaKedua = "5"
	var angkaKetiga = "6"
	var angkaKeempat = "7"

	intAngkaPertama, _ := strconv.Atoi(angkaPertama)
	intAngkaKedua, _ := strconv.Atoi(angkaKedua)
	intAngkaKetiga, _ := strconv.Atoi(angkaKetiga)
	intAngkaKeempat, _ := strconv.Atoi(angkaKeempat)

	fmt.Println(intAngkaPertama + intAngkaKedua + intAngkaKetiga + intAngkaKeempat)

	// soal 5
	fmt.Println("-------SOAL 5-------")
	kalimat := "halo halo bandung"
	angka := 2021

	kalimat = `"` + strings.ReplaceAll(kalimat, "halo", "Hi") + `" - ` + strconv.Itoa(angka)

	fmt.Println(kalimat)

}
